package com.lu3in033.projet.parser.combinators;

public class Location {
    public final long line;
    public final long column;

    public Location(long l, long c) {
        line = l;
        column = c;
    }
}
