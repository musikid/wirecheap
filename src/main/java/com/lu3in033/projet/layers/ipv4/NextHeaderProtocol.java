package com.lu3in033.projet.layers.ipv4;

public class NextHeaderProtocol {
    public final byte protocol;

    public NextHeaderProtocol(byte protocol) {
        this.protocol = protocol;
    }

}
